<!DOCTYPE html>
<html lang="en" dir="ltr">
 @include('menu')
  <body>
    <h3 class="text-blue center" >Administrar usuarios</h3>
    <div class="container ">
      <div class="row">
        <div class="col s11">
        </div>
        <a class="rigth btn-floating pulse" href="{{ url('/NuevoUsuario') }}"><i class="material-icons">add</i></a>
      </div>
      <table class="z-depth-3 highlight centered">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>Nickname</th>
              <th>Te Uniste</th>
              <th>Opciones</th>

          </tr>
        </thead>
        <tbody>
          @foreach ($usuario as $usuarios)
          <tr>
            <td> {{ $usuarios->nombre }} {{ $usuarios->apellido }}</td>
            <td>  {{ $usuarios->nickname }} </td>
            <td> {{ $usuarios->fecha }} </td>
            <td class="row">
              <a href="{{ url('/'.$usuarios->id.'/EditarUsuario') }}" class="btn teal col l6 m12">Editar</a>
            <form class="col l6 s12" action="{{ url('/'.$usuarios->id.'/EliminarUsuario' )}}" method="post">
              @csrf
              <input class="red btn text-white col s12" type="submit" name="" value="Eliminar" onclick="return confirm('esta seguro ?')">
            </form>
          </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <script type="text/javascript" src="js/materialize.min.js"></script>
  </body>
</html>
