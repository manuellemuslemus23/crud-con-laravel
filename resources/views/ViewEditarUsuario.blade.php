<!DOCTYPE html>
<html lang="en" dir="ltr">
  @include('menu')
  <body>
    <div class="container">
      <h4 class="teal-text center-align">Editar usuario {{ $user->nombre }}</h4>
      <form class="" action="{{ url('/'.$user->id . '/EditarUsuario') }}" method="post">
        @csrf

        <div class="input-field col s6">
          <input  type="text" name="nombre" value="{{ $user->nombre }}" class="validate" required>
          <label for="last_name">Nombre</label>
        </div>
        <div class="input-field col s6">
          <input  type="text" name="apellido" value="{{ $user->apellido }}" class="validate"  required>
          <label for="last_name">Apellido</label>
        </div>
        <div class="input-field col s6">
          <input  type="text" name="nickname" value="{{ $user->nickname }}" class="validate" required>
          <label for="last_name">Nickname</label>
        </div>

        <div class="form-group row justify-content-end">
          <div class="">
            <input type="submit" class=" btn btn-large col s12 "  name="" value="Guardar Cambios">
          </div>
        </div>
      </form>
    </div>
  </body>
</html>
