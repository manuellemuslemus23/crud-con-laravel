<?php /* /var/www/html/Crud-Laravel/resources/views/ViewRegistroUsuario.blade.php */ ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <?php echo $__env->make('menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
    <div class="container">
      <h4 class="teal-text center-align">Angregar nuevo usuario</h4>
      <form class="" action="<?php echo e(url('/NuevoUsuario')); ?>" method="post">
        <?php echo csrf_field(); ?>
        <div class="input-field col s6">
          <input  type="text" name="nombre" class="validate" required>
          <label for="last_name">Nombre</label>
        </div>
        <div class="input-field col s6">
          <input  type="text" name="apellido" class="validate"  required>
          <label for="last_name">Apellido</label>
        </div>
        <div class="input-field col s6">
          <input  type="text" name="nickname" class="validate" required>
          <label for="last_name">Nickname</label>
        </div>

        <div class="form-group row justify-content-end">
          <div class="">
            <input type="submit" class=" btn btn-large col s12 "  name="" value="Agregar Usuario">
          </div>
        </div>
      </form>
    </div>

  </body>
</html>
