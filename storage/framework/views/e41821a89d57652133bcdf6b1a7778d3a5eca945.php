<?php /* /var/www/html/Crud-Laravel/resources/views/ViewEditarUsuario.blade.php */ ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <?php echo $__env->make('menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
    <div class="container">
      <h4 class="teal-text center-align">Editar usuario <?php echo e($user->nombre); ?></h4>
      <form class="" action="<?php echo e(url('/'.$user->id . '/EditarUsuario')); ?>" method="post">
        <?php echo csrf_field(); ?>

        <div class="input-field col s6">
          <input  type="text" name="nombre" value="<?php echo e($user->nombre); ?>" class="validate" required>
          <label for="last_name">Nombre</label>
        </div>
        <div class="input-field col s6">
          <input  type="text" name="apellido" value="<?php echo e($user->apellido); ?>" class="validate"  required>
          <label for="last_name">Apellido</label>
        </div>
        <div class="input-field col s6">
          <input  type="text" name="nickname" value="<?php echo e($user->nickname); ?>" class="validate" required>
          <label for="last_name">Nickname</label>
        </div>

        <div class="form-group row justify-content-end">
          <div class="">
            <input type="submit" class=" btn btn-large col s12 "  name="" value="Guardar Cambios">
          </div>
        </div>
      </form>
    </div>
  </body>
</html>
