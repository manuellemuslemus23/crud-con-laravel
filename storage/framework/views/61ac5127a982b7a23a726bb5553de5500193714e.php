<?php /* /var/www/html/Crud-Laravel/resources/views/ViewHome.blade.php */ ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
 <?php echo $__env->make('menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
    <h3 class="text-blue center" >Administrar usuarios</h3>
    <div class="container ">
      <div class="row">
        <div class="col s11">
        </div>
        <a class="rigth btn-floating pulse" href="<?php echo e(url('/NuevoUsuario')); ?>"><i class="material-icons">add</i></a>
      </div>
      <table class="z-depth-3 highlight centered">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>Nickname</th>
              <th>Te Uniste</th>
              <th>Opciones</th>

          </tr>
        </thead>
        <tbody>
          <?php $__currentLoopData = $usuario; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuarios): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <tr>
            <td> <?php echo e($usuarios->nombre); ?> <?php echo e($usuarios->apellido); ?></td>
            <td>  <?php echo e($usuarios->nickname); ?> </td>
            <td> <?php echo e($usuarios->fecha); ?> </td>
            <td class="row">
              <a href="<?php echo e(url('/'.$usuarios->id.'/EditarUsuario')); ?>" class="btn teal col l6 m12">Editar</a>
            <form class="col l6 s12" action="<?php echo e(url('/'.$usuarios->id.'/EliminarUsuario' )); ?>" method="post">
              <?php echo csrf_field(); ?>
              <input class="red btn text-white col s12" type="submit" name="" value="Eliminar" onclick="return confirm('esta seguro ?')">
            </form>
          </td>
          </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
    <script type="text/javascript" src="js/materialize.min.js"></script>
  </body>
</html>
