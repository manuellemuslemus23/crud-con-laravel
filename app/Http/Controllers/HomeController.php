<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\usuario;

class HomeController extends Controller
{

    public function MostrarHome()
    {
      $usuario = usuario::all(); //array
      // //$alumnos = alumno::all(); //array
      // $matricula = matricula::all();
      // $valoresunicos = $matricula->unique('idano');
      // $valoresunicos->values()->all();
      // return view('Matricula')->with(compact('grados','valoresunicos'));
      return view('ViewHome')->with(compact('usuario'));
    }
}
