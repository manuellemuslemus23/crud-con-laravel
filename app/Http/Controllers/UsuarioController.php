<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\usuario;

class UsuarioController extends Controller
{
    public function RegistroUsuario()
    {
      return view('ViewRegistroUsuario');
    }
    public function GuardandoUsuario(Request $request)
    {
      $user = new usuario();
      $user->nombre = $request->input('nombre');
      $user->apellido = $request->input('apellido');
      $user->nickname = $request->input('nickname');
      $user->save();

      return redirect('/');
    }

    public function EliminarUsuario($id)
    {

      $user = usuario::find($id);
      $user->delete();
      return redirect('/');
    }
    public function FormularioEditarUsuario($id)
    {
      $user = usuario::find($id);

      return view('ViewEditarUsuario')->with(compact('user'));
    }
    public function ModificarUsuario(Request $request, $id)
    {
      $user = usuario::find($id);
      $user->nombre = $request->input('nombre');
      $user->apellido = $request->input('apellido');
      $user->nickname = $request->input('nickname');
      $user->save();
      return redirect('/');
    }

}
