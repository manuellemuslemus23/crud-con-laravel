<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario extends Model
{
  protected $table = 'Tusuario';
  public $timestamps = false;

  protected $fillable = [
        'nombre', 'apellido', 'nickname', 'fecha',
    ];
}
