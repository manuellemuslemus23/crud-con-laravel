# CRUD con Laravel

Incertar, mostrar, modificar y eliminar registros a una base de datos Mysql con Laravel.

### Requisitos

- php 7.1 >
- mysql
- apache2
- Composer


## Instrucciones

1) crear una base de datos en mysql.

2) luego importar el archivo usuariodb.sql ha la base de datos creada en Mysql.

3) modificar los siguientes campos del archivo .env

   DB_DATABASE = nombre_de_la_base_de_datos

   DB_USERNAME = usuario_de_la_base_de_datos
   
   DB_PASSWORD = contrasena_de_usuario_de_base_de_datos


4)ubicarse en la carpeta que contiene los archivos, luego abrir una terminal y ejecutar

```bash
php artisan serve
```

